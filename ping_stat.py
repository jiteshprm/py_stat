#!/usr/bin/env python2
from ping import ping_with_average
import socket
import urllib2
import time
import json
import subprocess
import platform
import re


def get_ip_internal_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def do_ping_stat(hostname, current_internal_ip, current_external_ip, current_time_epoch, current_time_human, current_vpn_on, current_platform, link_quality, signal_level, noise_level):
    (dest_addr, host_ip, status_human, timeout, count, status_boolean, status_msg, ping_avg, pings) = ping_with_average(hostname)

    responsejson = dict()
    responsejson['version'] = "1"
    responsejson['time_epoch'] = current_time_epoch
    responsejson['time_human'] = current_time_human
    responsejson['ip_internal'] = current_internal_ip
    responsejson['ip_external'] = current_external_ip
    responsejson['ping_hostname'] = dest_addr
    responsejson['ping_ip'] = host_ip
    responsejson['status_boolean'] = status_boolean
    responsejson['status_msg'] = status_msg
    responsejson['status_human'] = status_human
    responsejson['count'] = count
    responsejson['ping_avg'] = ping_avg
    responsejson['pings'] = pings
    responsejson['vpn_on'] = current_vpn_on
    responsejson['platform'] = current_platform
    responsejson['link_quality'] = link_quality
    responsejson['signal_level'] = signal_level
    responsejson['noise_level'] = noise_level

    json_encode_string = json.dumps(responsejson)

    f = open(stats_file_path, 'a')
    f.write("{time_epoch}={json_encode_string}\n".format(time_epoch=current_time_epoch, json_encode_string=json_encode_string))
    f.close()


def get_external_ip():
    req = urllib2.Request('https://checkip.amazonaws.com', data=None)
    response = urllib2.urlopen(req, timeout=5)
    my_external_ip = response.read()
    my_external_ip_filtered = str(my_external_ip).replace("\n","")
    # print(my_external_ip)
    return my_external_ip_filtered


def get_time_epoch():
    ts = time.time() * 1000000L
    tsf = '{:.0f}'.format(ts)
    # print(tsf)
    return tsf


def get_vpn_is_on():
    cmd = ['ifconfig', vpn_adapter_name]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    o, e = proc.communicate()
    exists_adapter = vpn_adapter_name in str(o.decode('ascii'))
    # print (exists_adapter)
    # print('Output: ' + o.decode('ascii'))
    # print('Error: '  + e.decode('ascii'))
    # print('code: ' + str(proc.returncode))
    return exists_adapter


def get_platform():
    return platform.system()


def get_time_human(time_epoch):
    return time.strftime('%c %Z', time.gmtime(float(time_epoch) / 1000000.0))


def get_signal_linux():
    cmd = ['/sbin/iwconfig', wlan_adapter_name]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    o, e = proc.communicate()
    link_quality = re.search(r"Link Quality=(\w+)", str(o.decode('ascii')), re.MULTILINE).group(1)
    signal_level = re.search(r"Signal level=(.\w+)", str(o.decode('ascii')), re.MULTILINE).group(1)
    noise_level = re.search(r"Noise level=(\w+)", str(o.decode('ascii')), re.MULTILINE).group(1)
    # print (signal)
    # print('Output: ' + o.decode('ascii'))
    # print('Error: '  + e.decode('ascii'))
    # print('code: ' + str(proc.returncode))
    return link_quality, signal_level, noise_level


def get_signal_darwin():
    return 0, 0, 0


if __name__ == '__main__':
    print("Started at " + time.strftime('%c %Z'))
    if "Darwin" in get_platform():
        stats_file_path = "./stats_icmp.txt"
        vpn_adapter_name = "tun0"
        wlan_adapter_name = "wlan0"
    else:
        stats_file_path = "/hdd/ping_stat/stats_icmp.txt"
        vpn_adapter_name = "tun0"
        wlan_adapter_name = "wlan0"

    current_internal_ip = get_ip_internal_address()
    current_external_ip = get_external_ip()

    current_time_epoch = get_time_epoch()
    current_time_human = get_time_human(current_time_epoch)

    current_vpn_on = get_vpn_is_on()
    current_platform = get_platform()

    if "Darwin" in get_platform():
        link_quality, signal_level, noise_level = get_signal_darwin()
    else:
        link_quality, signal_level, noise_level = get_signal_linux()

    do_ping_stat('www.google.com', current_internal_ip, current_external_ip, current_time_epoch, current_time_human, current_vpn_on, current_platform, link_quality, signal_level, noise_level)
    do_ping_stat('127.0.0.1', current_internal_ip, current_external_ip, current_time_epoch, current_time_human, current_vpn_on, current_platform, link_quality, signal_level, noise_level)
    do_ping_stat('iptv.rent', current_internal_ip, current_external_ip, current_time_epoch, current_time_human, current_vpn_on, current_platform, link_quality, signal_level, noise_level)
    print("Finished OK at " + time.strftime('%c %Z'))
